<?php

namespace Calendar;


class Events{

    private $pdo;

    public function __construct(\PDO $pdo){
        $this->pdo = $pdo;
    }

/**
 * Permet de récupérer tout les events entre deux dates
 * @param \DateTimeInterface $start
 * @param \DateTimeInterface $end
 * @return Event[]
 */
    public function getEventsBetween (\DateTimeInterface $start, \DateTimeInterface $end): array{
        $sql = "SELECT * FROM events WHERE start BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}' ORDER BY start DESC";
        $statement = $this->pdo->query($sql);
        $statement->setFetchMode(\PDO::FETCH_CLASS, Event::class);
        $results = $statement->fetchAll();
        return $results;
    }
/**
 * Permet de récupérer tout les events entre deux dates indexé par jour
 * @param \DateTimeInterface $start
 * @param \DateTimeInterface $end
 * @return array
 */
    public function getEventsBetweenByDay(\DateTimeInterface $start, \DateTimeInterface $end): array{
        $events = $this->getEventsBetween($start, $end);
        $days = [];
        foreach($events as $event){
            // On récupère la date, exemple : "2018-07-23"
            $date = $event->getStart()->format('Y-m-d');
            // Si elle n'est pas encore dans $days, on créé un tableau de taille 1 qui contient notre évènement
            if (!isset($days[$date])) {
                $days[$date] = [$event];
            // Sinon on l'ajoute dans $days
            } else {
                $days[$date][] = $event;
            }
        }
        return $days;
    }

/**
 * Récupère un event par un id
 * @param int $id
 * @return Event
 * @throws \Exception
 */
    public function find(int $id): Event{
        require 'Event.php';
        $statement = $this->pdo->query("SELECT * FROM events WHERE id = $id LIMIT 1");
        $statement->setFetchMode(\PDO::FETCH_CLASS, Event::class);
        $result = $statement->fetch();
        if ($result === false){
            throw new \Exception('Aucun résultat n\'a été trouvé');
        } else {
            return $result;
        }
    }

    public function hydrate(Event $event, array $data): Event{
        $event->setName($data['name']);
        $event->setDescription($data['description']);
        $event->setStart(\DateTime::CreateFromFormat('Y-m-d H:i', $data['date'] . ' ' . $data['start'])->format('Y-m-d H:i:s'));
        $event->setEnd(\DateTime::CreateFromFormat('Y-m-d H:i', $data['date'] . ' ' . $data['end'])->format('Y-m-d H:i:s'));
        return $event;
    }

    /**
     * Insertion d'un Event dans la BDD depuis un objet Event donné en paramètre
     *
     * @param Event $event
     * @return boolean
     */
    public function create(Event $event): bool{
        $statement = $this->pdo->prepare('INSERT INTO events (name, description, start, end) VALUES (?, ?, ?, ?)');
        $statement->execute([
            $event->getName(),
            $event->getDescription(),
            $event->getStart()->format('Y-m-d H:i:s'),
            $event->getEnd()->format('Y-m-d H:i:s'),
        ]);
        return true;
    }

    /**
     * Met à jour un évènement au niveau de la base de donnée
     *
     * @param Event $event
     * @return boolean
     */
    public function update(Event $event): bool{
        $statement = $this->pdo->prepare('UPDATE events SET name = ?, description = ?, start = ?, end = ? WHERE id = ?');
        $statement->execute([
            $event->getName(),
            $event->getDescription(),
            $event->getStart()->format('Y-m-d H:i:s'),
            $event->getEnd()->format('Y-m-d H:i:s'),
            $event->getId()
        ]);
        return true;
    }

}