<?php

namespace Calendar;

class Month {
	public $days = [
		'Lundi',
		'Mardi',
		'Mercredi',
		'Jeudi',
		'Vendredi',
		'Samedi',
		'Dimanche'
	];
	private $months = [
		'Janvier',
		'Février',
		'Mars',
		'Avril',
		'Mai',
		'Juin',
		'Juillet',
		'Août',
		'Septembre',
		'Octobre',
		'Novembre',
		'Décembre'
	];
	public $month;
	public $year;

/**
 * @param int $month Le mois compris entre 1 et 2
 * @param int $year L'année
 * @throws \Exception
 */

    public function __construct(?int $month = null, ?int $year = null) {
		if ($month === null){
			$month = intval(date('m'));
		}
		if ($year === null){
			$year = intval(date('Y'));
		}
        if ($month < 1 || $month > 12) {
            throw new \Exception("Le mois $month n'est pas valide");
        }
        if ($year < 1970) {
            throw new \Exception("L'année est inférieure à 1970");
		}
		$this->month = $month;
		$this->year = $year;
	}

/**
 * Retourne le mois en toute lettres (ex : Mars 2018)
 * @return string
 */
	public function toString(): string {
		return $this->months[$this->month - 1] . ' ' . $this->year;
	}


/**
 * Renvoie le 1er jour du mois
 * @return \DateTimeImmutable
 */
	public function getStartingDay(): \DateTimeInterface{
		return $start = new \DateTimeImmutable("{$this->year}-{$this->month}-01");
	}

/**
 * Retourne le nombre de semaine qu'il y a dans ce mois
 * @return int
 */
	public function getWeeks(): int {
		$start = $this->getStartingDay();					// Date du 1er jour du mois
		$end = $start->modify('+1 month -1 day');	// Date du dernier jour du mois
		$startWeek = intval($start->format('W'));			// Numéro de première semaine du mois, exemple : décembre 48
		$endWeek = intval($end->format('W'));				// Numéro de dernière semaine du mois, bug : 1 pour décembre
		if ($endWeek === 1){
			// Si la dernière semaine de l'année = 1
			// On clone la date du dernier jour du mois
			// On enlève 7 jours, mais on rajoute + 1 semaine, ce qui donne 53 au lieu de 1
			$endWeek = intval($end->modify('- 7 days')->format('W')) + 1;
		}
		$weeks = $endWeek - $startWeek + 1;
		if ($weeks < 0){
			$weeks = $endWeek;
		}
		return $weeks;
	}

/**
 * Est-ce que le jour est dans le mois en cours
 * @param \DateTimeInterface $date
 * @return bool
 */
	public function withinMonth (\DateTimeInterface $date): bool {
		return $this->getStartingDay()->format('Y-m') === $date->format('Y-m');
	}

/**
 * Renvoie le mois suivant
 * @return Month
 */
	public function nextMonth(): Month {
		$month = $this->month + 1;
		$year = $this->year;
		if ($month > 12){
			$month = 1;
			$year += 1;
		}
		return new Month($month, $year);
	}

/**
 * Renvoie le mois précédent
 * @return Month
 */
public function previousMonth(): Month {
	$month = $this->month - 1;
	$year = $this->year;
	if ($month < 1){
		$month = 12;
		$year -= 1;
	}
	return new Month($month, $year);
}
}