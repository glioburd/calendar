<?php

require('debug.php');
require('../vendor/autoload.php');

function e404(){
    require('../public/404.php');
    exit();
}

function get_PDO(): PDO{
    return new PDO('mysql:host=localhost;dbname=tutocalendar', 'root', 'root', [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ]);
}

function h($value): string{
    if ($value == null){
        return '';
    }
    return htmlentities($value);
}

function render (string $view, $parameters = []){
    extract($parameters);
    include "../views/{$view}.php";
}