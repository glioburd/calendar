<?php

/**
 * @param mixed $data
 */
function ppp($data)
{
    dieData($data, false);
}

/**
 * @param mixed $data
 */
function ddd($data)
{
    dieData($data, true);
}

/**
 * @param mixed $data
 * @param bool $kill
 *
 * @return mixed
 */
function dieData($data, $kill = true)
{
    echo '<pre style="text-align: left;">';
    print_r($data);
    echo '</pre><br />';

    if ($kill) {
        die('END');
    }

    return $data;
}

/**
 * @param mixed $data
 */
function vppp($data)
{
    vdieData($data, false);
}

/**
 * @param mixed $data
 */
function vddd($data)
{
    vdieData($data, true);
}

/**
 * @param mixed $data
 * @param bool $kill
 *
 * @return mixed
 */
function vdieData($data, $kill = true)
{
    echo '<pre style="text-align: left;">';
    var_dump($data);
    echo '</pre><br />';

    if ($kill) {
        die('END');
    }

    return $data;
}
