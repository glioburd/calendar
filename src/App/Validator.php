<?php

namespace App;

class Validator{
    
    private $data;
    protected $errors = [];

    public function __construct($data = []){
        $this->data = $data;
    }

    /**
     * validates
     *
     * @param array $data
     * @return array|bool
     */
    public function validates(array $data){
        $this->errors = [];
        $this->data = $data;
        return $this->errors;
    }

    /**
     * Check si le champs est rempli, s'il l'est, check des erreurs avec la méthode choisie
     *
     * @param string $field
     * @param string $method
     * @param [type] ...$parameters
     * @return bool
     */
    public function validate(string $field, string $method, ...$parameters): bool{
        if(!isset($this->data[$field])){
            $this->errors[$field] = "Le champs $field n'est pas rempli.";
            return false;
        } else {
            return call_user_func([$this, $method], $field, ...$parameters);
        }
    }

    /**
     * Check min length
     *
     * @param string $field
     * @param integer $length
     * @return void
     */
    public function minLength(string $field, int $length): bool {
        if(strlen($this->data[$field]) < $length) {
            $this->errors[$field] = "Le champs doit avoir plus de $length caractères";
            return false;
        }
        return true;
    }

    /**
     * Check si le paramètre est bien un DateTime Y-m-d valide
     *
     * @param string $field
     * @return boolean
     */
    public function date(string $field): bool {
        if (\DateTime::createFromFormat('Y-m-d', $this->data[$field]) === false) {
            $this->errors[$field] = "La date n'est pas valide.";
            return false;
        }
        return true;
    }

    /**
     * Check si le paramètre est bien un DateTime H:i valide
     *
     * @param string $field
     * @return boolean
     */
    public function time(string $field): bool {
        if (\DateTime::createFromFormat('H:i', $this->data[$field]) === false) {
            $this->errors[$field] = "Le temps n'est pas valide.";
            return false;
        }
        return true;
    }

    /**
     * Check si la date de départ et la date de fin sont des dates
     * Puis check si la date de départ est inférieure à la date de fin
     *
     * @param string $startField
     * @param string $endField
     * @return boolean
     */
    public function beforeTime(string $startField, string $endField): bool{
        if ($this->time($startField) && $this->time($endField)){
            $start = \DateTime::createFromFormat('H:i', $this->data[$startField]);
            $end = \DateTime::createFromFormat('H:i', $this->data[$endField]);
            if ($start->getTimestamp() > $end->getTimestamp()) {
                $this->errors[$startField] = "Le temps de départ doit être inférieur au temps d'arrivé.";
                return false;
            }
            return true;
        }
        return false;
    }
}