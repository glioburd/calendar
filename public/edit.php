<?php

require "../src/lib/bootstrap.php";

$pdo = get_PDO();

if (!isset($_GET['id'])){
    e404();
}

$events = new Calendar\Events($pdo);
$errors = [];

try {
    $event = $events->find($_GET['id'] ?? null);
} catch (\Exception $e) {
    e404();
} catch (\Error $e){
    e404();
}

$data = [
    'name' => $event->getName(),
    'date' => $event->getStart()->format('Y-m-d'),
    'start' => $event->getStart()->format('H:i'),
    'end' => $event->getEnd()->format('H:i'),
    'description' => $event->getDescription()
];

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = $_POST;
    $errors = [];
    $validator = new Calendar\EventValidator();
    $errors = $validator->validates($data);
    if (empty($errors)){
        $events->hydrate($event, $data);
        $events->update($event);
        header('Location: /index.php?success=1');
        exit();
    }
}

render("header", ['title' => $event->getName()]);

?>

<h1>Editer l'évènement <small><?= h($event->getName()); ?></small></h1>
<div class="container">
    <?php if(!empty($errors)): ?>
        <div class="alert alert-danger">
            Merci de corriger vos erreurs.
            <?php ppp($errors); ?>
        </div>
    <?php endif; ?>
    <form action="" method="post">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">Titre</label>
                    <input id="name" type="text" required class="form-control" name="name" value="<?= isset($data['name']) ? h($data['name']) : "Titre par défaut" ?>">
                    <?php if(isset($errors['name'])): ?>
                        <p class="help-block"><?= $errors['name']; ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="date">Date</label>
                    <input id="date" type="date" required class="form-control" name="date" value="<?= isset($data['date']) ? h($data['date']) : "" ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="start">Heure de départ</label>
                    <input id="start" type="time" required class="form-control" name="start" placeholder="HH:mm" value="<?= isset($data['start']) ? h($data['start']) : "" ?>">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="end">Heure de fin</label>
                    <input id="end" type="time" required class="form-control" name="end" placeholder="HH:mm" value="<?= isset($data['end']) ? h($data['end']) : "" ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" id="description" class="form-control"><?= isset($data['description']) ? h($data['description']) : "" ?></textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Modifier l'évènement</button>
        </div>
    </form>
</div>
<?php render('footer'); ?>