<?php
    require "../src/lib/bootstrap.php";
    use Calendar\{
        Events,
        Month
    };

    $pdo = get_PDO();
    $month = new Month($_GET['month'] ?? null, $_GET['year'] ?? null);
    $weeks = $month->getWeeks();
    $start = $month->getStartingDay();
    // Si le mois commence par un lundi, on envoie direct le 1er jour du mois. Sinon, on récupère le lundi d'avant.
    $start = $start->format('N') === '1' ? $start : $month->getStartingDay()->modify('last monday');
    $end = $start->modify('+' . (6 + 7 * ($weeks -1)) . 'days');
    $events = new Events($pdo);
    $events = $events->getEventsBetweenByDay($start, $end);
    render("header");
?>

<div class="calendar">

    <?php if(isset($_GET['success'])): ?>
        <div class="container">
            <div class="alert alert-success">
                Opération réussie !
            </div>
        </div>
    <?php endif ?>

    <div class="d-flex flex-row align-items-center justify-content-between mx-sm-3">
        <h1><?= $month->toString(); ?></h1>
        <div>
        <a href="./index.php?month=<?= $month->previousMonth()->month ?>&year=<?= $month->previousMonth()->year ?>" class="btn btn-primary">&lt;</a>
        <a href="./index.php?month=<?= $month->nextMonth()->month ?>&year=<?= $month->nextMonth()->year ?>" class="btn btn-primary">&gt;</a>
    </div>
    </div>

    <table class="calendar__table calendar__table--<?= $weeks; ?>weeks">
        <?php for($i = 0; $i < $weeks; $i++): ?>
            <tr>
                <?php foreach($month->days as $k => $day): 
                    $date = $start->modify("+" . ($k + $i * 7) . "days");
                    // Pour aujourd'hui, on prend dans $todayEvents les évènements de $events qui sont dans la clé de la date d'aujourd'hui.
                    // Si ça n'existe pas, on créé un tableau vide.
                    $todayEvents = $events[$date->format('Y-m-d')] ?? [];
                    $isToday = date('Y-m-d') === $date->format('Y-m-d');
                ?>
                    <td class="<?= $month->withinMonth($date) ? '' : 'calendar__othermonth'; ?> <?= $isToday ? 'is-today' : ''?>">
                        <?php if ($i === 0): ?>
                            <div class="calendar__weekday"><?= $day; ?></div>
                        <?php endif ?>
                        <a class="calendar__day" href="add.php?date=<?= $date->format('Y-m-d') ?>"><?= $date->format('d'); ?></a>
                        <?php foreach($todayEvents as $todayEvent): ?>
                            <div class="calendar__event">
                            <a href="event.php?id=<?= $todayEvent->getId(); ?>"><?= $todayEvent->getName(); ?></a>
                            <?= $todayEvent->getStart()->format('H:i')?> - <a href="/edit.php?id=<?= $todayEvent->getId()?>">✎ edit</a>
                            </div>
                        <?php endforeach ?>
                    </td>
                <?php endforeach ?>
            </tr>
        <?php endfor ?>
    </table>

    <a href="add.php" class="calendar__button">+</a>
</div>

<?php require "../views/footer.php"; ?>