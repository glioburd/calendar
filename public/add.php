<?php

require '../src/lib/bootstrap.php';

//On initialise un tableau vide de data, sauf date si on créé un évènement depuis le jour d'un calendrier avec ?date=
$data = [
    'date' => $_GET['date'] ?? date('Y-m-d'),
    'start' => date('H:i'),
    'end' => date('H:i')
];
$validator = new \App\Validator($data);
if (!$validator->validate('date', 'date')){
    $data['date'] = date('Y-m-d');
}
$errors = [];
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = $_POST;
    $validator = new Calendar\EventValidator();
    $errors = $validator->validates($_POST);
    if (empty($errors)){
        $event = $events->hydrate(new \Calendar\Event(), $data);
        $events = new \Calendar\Events(get_pdo());
        $events->create($event);
        header('Location: /index.php?success=1');
        exit();
    }
}

render('header', ['title' => 'Ajouter un évènement']);
?>

<div class="container">

    <?php if(!empty($errors)): ?>
        <div class="alert alert-danger">
            Merci de corriger vos erreurs.
            <?php ppp($errors); ?>
        </div>
    <?php endif; ?>

    <h1>Ajouter un évènement</h1>
    <form action="" method="post">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="name">Titre</label>
                    <input id="name" type="text" required class="form-control" name="name" value="<?= isset($data['name']) ? h($data['name']) : "Titre par défaut" ?>">
                    <?php if(isset($errors['name'])): ?>
                        <p class="help-block"><?= $errors['name']; ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="date">Date</label>
                    <input id="date" type="date" required class="form-control" name="date" value="<?= isset($data['date']) ? h($data['date']) : "" ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="start">Heure de départ</label>
                    <input id="start" type="time" required class="form-control" name="start" placeholder="HH:mm" value="<?= isset($data['start']) ? h($data['start']) : "" ?>">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="end">Heure de fin</label>
                    <input id="end" type="time" required class="form-control" name="end" placeholder="HH:mm" value="<?= isset($data['end']) ? h($data['end']) : "" ?>">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" id="description" class="form-control"><?= isset($data['description']) ? h($data['description']) : "" ?></textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Ajouter l'évènement</button>
        </div>
    </form>
</div>



<?php render('footer'); ?>