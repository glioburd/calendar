-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `events` (`id`, `name`, `description`, `start`, `end`) VALUES
(1,	'Evenement',	'',	'2018-07-23 23:00:00',	'2018-07-23 23:55:00'),
(2,	'Evenement 2',	'',	'2018-07-24 23:00:00',	'2018-07-24 23:55:00'),
(3,	'Evenement 3',	'<h1>Bonjour c\'est la description</h1>',	'2018-07-24 23:00:00',	'2018-07-24 23:55:00');

-- 2018-08-10 15:46:53