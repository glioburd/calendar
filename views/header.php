<!Doctype html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="css/calendar.css">
    <title><?= isset($title) ? h($title) : 'Mon calendrier'; ?></title>
</head>
<body>

<nav class="navbar navbar-dark bg-primary mb-3">
    <a href="index.php" class="navbar-brand">Mon calendrier</a>
</nav>